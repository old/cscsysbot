package utils

func InList(l []string, s string) bool {
   for _, i := range l {
      if i == s {
         return true
      }
   }

   return false
}
