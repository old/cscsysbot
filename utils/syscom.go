package utils

import(
   "crypto/tls"
   "fmt"
   "strings"
   "sort"

   "gopkg.in/ldap.v2"
)

type InsensitiveSlice []string
func (s InsensitiveSlice) Len() int {
   return len(s)
}

func (s InsensitiveSlice) Swap(i, j int) {
   s[i], s[j] = s[j], s[i]
}

func (s InsensitiveSlice) Less(i, j int) bool {
   a := strings.ToLower(s[i])
   b := strings.ToLower(s[j])

   return a < b
}

const (
   ldapServer = "ldap-master.csclub.uwaterloo.ca"
)

func SyscomNicks() ([]string, error) {
    l, err := ldap.DialTLS("tcp", fmt.Sprintf("%s:%d", ldapServer, 636), &tls.Config{
      ServerName: ldapServer,
   })
   if err != nil {
      return make([]string, 0), err
   }
   defer l.Close()

   // Get members
   req := ldap.NewSearchRequest("ou=Group,dc=csclub,dc=uwaterloo,dc=ca", ldap.ScopeWholeSubtree,
                               ldap.NeverDerefAliases, 0, 0, false,
                               "(&(cn=syscom)(objectClass=posixGroup))",
                               []string{"*"}, nil)
   res, err := l.Search(req)
   if err != nil {
      return make([]string, 0), err
   }

   if len(res.Entries) > 0 {
      entry := res.Entries[0]

      members := entry.GetAttributeValues("uniqueMember")
      memberUids := make([]string, len(members))
      for i, member := range members {
         memberUids[i] = strings.Replace(strings.Replace(member, "uid=", "", -1),
                           ",ou=People,dc=csclub,dc=uwaterloo,dc=ca", "", -1)

         // Exceptions: some use different nicks
         switch memberUids[i] {
            case "ztseguin":
               memberUids[i] = "zseguin"
            case "nablack":
               memberUids[i] = "Na"
            case "laden":
               memberUids[i] = "Luqman"
            case "c7zou":
               memberUids[i] = "gcrl"
            case "abandali":
               memberUids[i] = "bandali"
            case "r389li":
               memberUids[i] = "raymo"
         }
      }

      smemberUids := InsensitiveSlice(memberUids[0:])
      sort.Sort(smemberUids)
      return []string(smemberUids), nil
   }

   return make([]string, 0), nil
}
