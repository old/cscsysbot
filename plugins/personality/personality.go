package personality

import (
   "fmt"
   "math/rand"
   "regexp"

   "github.com/go-chat-bot/bot"
)

const (
   botPattern = "(?i)cscsysbot"
   thanksPattern = "(?i)(thanks?|thank you|thx)"
   negativePattern = "(?i)(you suck|u suck|you're (wrong|incorrect)|ur (wrong|incorrect)|that'?s (wrong|incorrect))"
   whyPattern = "(?i)why"
)

var (
   botRe = regexp.MustCompile(botPattern)

   thanksRe = regexp.MustCompile(thanksPattern)
   thanksResponses = []string{
      "You're welcome %s",
      "It's my pleasure %s",
      "I'm here to serve you %s",
      "No problem %s",
      "Anytime %s",
   }

   negativeRe = regexp.MustCompile(negativePattern)
   negativeResponses = []string{
      "I'm sorry to dissapoint you %s",
      "Sorry %s",
      "I promise I'll do better next time %s",
   }

   whyRe = regexp.MustCompile(whyPattern)
   whyResponses = []string{
      "That is a good question %s",
      "I don't know why %s",
      "I'm unsure as well %s",
   }
)

func responseTemplate(re *regexp.Regexp, responses []string) (func(*bot.PassiveCmd) (string, error)) {
   return func(command *bot.PassiveCmd) (string, error) {
      if (command.ChannelData.IsPrivate || botRe.MatchString(command.Raw)) && re.MatchString(command.Raw) {
         return fmt.Sprintf(responses[rand.Intn(len(responses))], command.User.Nick), nil
      }
      return "", nil
   }
}

func init() {
   bot.RegisterPassiveCommand(
      "personality_thanks",
      responseTemplate(thanksRe, thanksResponses))

   bot.RegisterPassiveCommand(
      "personality_negative",
      responseTemplate(negativeRe, negativeResponses))

   bot.RegisterPassiveCommand(
      "personality_why",
      responseTemplate(whyRe, whyResponses))
}
