package tweets

import (
   "fmt"
   "log"
   "os"
   "strings"

   "git.uwaterloo.ca/csc/cscsysbot/plugins/background"

   "github.com/dghubble/go-twitter/twitter"
   "github.com/dghubble/oauth1"
)

func lookup(tw *twitter.Client, sn string) (*twitter.User, error) {
   params := &twitter.UserLookupParams{
      ScreenName: []string{sn},
   }
   users, _, err := tw.Users.Lookup(params)

   if err != nil {
      return nil, err
   }

   if len(users) > 0 {
      return &users[0], nil
   } else {
      return nil, nil
   }
}

func userInList(users []*twitter.User, user *twitter.User) bool {
   for _, u := range users {
      if u.ID == user.ID {
         return true
      }
   }

   return false
}

func init() {
   // Start Twitter stream
   authConfig := oauth1.NewConfig(os.Getenv("TWITTER_CONSUMER_KEY"), os.Getenv("TWITTER_CONSUMER_SECRET"))
   authToken := oauth1.NewToken(os.Getenv("TWITTER_ACCESS_TOKEN"), os.Getenv("TWITTER_ACCESS_SECRET"))

   httpClient := authConfig.Client(oauth1.NoContext, authToken)
   tw := twitter.NewClient(httpClient)

   watchScreenames := strings.Split(os.Getenv("TWITTER_USERS"), ",")
   var users []*twitter.User
   for _, screename := range watchScreenames {
      user, err := lookup(tw, screename)
      if err != nil {
         log.Println("Unable to get Twitter user", screename, err)
         continue
      }

      users = append(users, user)
   }

   demux := twitter.NewSwitchDemux()
   demux.Tweet = func(tweet *twitter.Tweet) {
      if userInList(users, tweet.User) {
         tweetText := strings.Replace(tweet.Text, "\n", " ", -1)

         // Replace t.co URLs
         for _, entity := range tweet.Entities.Urls {
            tweetText = strings.Replace(tweetText, entity.URL, entity.ExpandedURL, 1)
         }

         // Replace t.co media URLs
         for _, entity := range tweet.Entities.Media {
            url := entity.MediaURLHttps
            if (url == "") {
               url = entity.MediaURL
            }

            tweetText = strings.Replace(tweetText, entity.URL, url, 1)
         }

         background.Messages <- fmt.Sprintf("%s: %s [@%s]", tweet.User.Name, tweetText, tweet.User.ScreenName)
      }
   }
   demux.StreamDisconnect = func(disconnect *twitter.StreamDisconnect) {
      log.Println("Stream disconnected", disconnect)
   }

   userIds := make([]string, len(users))
   for i, user := range users {
      userIds[i] = user.IDStr
   }
   params := &twitter.StreamFilterParams{
      Follow: userIds,
      StallWarnings: twitter.Bool(true),
   }
   stream, err := tw.Streams.Filter(params)
   if err != nil {
      return
   }

   go demux.HandleChan(stream.Messages)
}
