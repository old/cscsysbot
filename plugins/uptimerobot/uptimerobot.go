package uptimerobot

import (
   "fmt"
   "time"
   "strings"

   ur "git.uwaterloo.ca/csc/cscsysbot/utils/uptimerobot"
   "git.uwaterloo.ca/csc/cscsysbot/plugins/background"

   "github.com/go-chat-bot/bot"
)

func uptimeRobot() {
   lastStatuses := make(map[int]ur.MonitorStatus)

   for {
      monitors, err := ur.GetMonitors()
      if err != nil {
         time.Sleep(time.Minute * 1)
         continue
      }

      for _, mon := range monitors.Monitors {
         lastStatus, ok := lastStatuses[mon.ID]
         lastStatuses[mon.ID] = mon.Status

         if !ok {
            continue
         }

         if lastStatus != mon.Status {
            if (mon.Type == ur.MonitorTypePort) {
               background.Messages <- fmt.Sprintf("Uptime Robot: %s -> %s, %s:%d (%s)\n", lastStatus, mon.Status, mon.URL, int(mon.Port.(float64)), mon.Type)
            } else {
               background.Messages <- fmt.Sprintf("Uptime Robot: %s -> %s, %s (%s)\n", lastStatus, mon.Status, mon.URL, mon.Type)
            }
         }

      }

      time.Sleep(time.Minute * 1)
   }
}

func inList(l []ur.MonitorStatus, s ur.MonitorStatus) bool {
   for _, i := range l {
      if i == s {
         return true
      }
   }

   return false
}

func monitorQuery(command *bot.Cmd) (string, error) {
   var lines []string
   var statuses []ur.MonitorStatus

   for _, arg := range command.Args {
      switch strings.ToLower(arg) {
         case "up":
            statuses = append(statuses, ur.MonitorStatusUp)
         case "down":
            statuses = append(statuses, ur.MonitorStatusDown)
         case "paused":
            statuses = append(statuses, ur.MonitorStatusPaused)
         case "seemsdown":
            fallthrough
         case "seems_down":
            statuses = append(statuses, ur.MonitorStatusSeemsDown)
         case "notchecked":
            fallthrough
         case "not_checked":
            statuses = append(statuses, ur.MonitorStatusNotCheckedYet)
         default:
            lines = append(lines, fmt.Sprintf("Unknown monitor status: %q", strings.ToLower(arg)))
      }
   }

   if len(statuses) == 0 {
      lines = append(lines, fmt.Sprintf("Please provide at least one status to filter with."))
      return strings.Join(lines, "\n"), nil
   }

   monitors, err := ur.GetMonitors()
   if err != nil {
      return "", nil
   }

   match := 0
   for _, mon := range monitors.Monitors {
      if !inList(statuses, mon.Status) {
         continue;
      }

      match++
      var status string

      if (mon.Type == ur.MonitorTypePort) {
         status = fmt.Sprintf("Uptime Robot: %s, %s:%d (%s)\n", mon.Status, mon.URL, int(mon.Port.(float64)), mon.Type)
      } else {
         status = fmt.Sprintf("Uptime Robot: %s, %s (%s)\n", mon.Status, mon.URL, mon.Type)
      }
      lines = append(lines, status)
   }

   if match == 0 {
      lines = append(lines, "No monitors matched the requested statuses")
   }

   return strings.Join(lines, "\n"), nil
}

func init() {
   go uptimeRobot()

   bot.RegisterCommand(
      "monitors",
      "Query Uptime Robot Monitors status",
      "Retricts monitors to those matching the list of provided statuses (ex: !monitors paused down  to show up and down monitors)",
      monitorQuery)
}
