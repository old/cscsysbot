package member

import (
   "crypto/tls"
   "fmt"
   "sort"
   "strconv"
   "strings"

   "git.uwaterloo.ca/csc/cscsysbot/utils"

   "github.com/go-chat-bot/bot"
   "gopkg.in/ldap.v2"
)

const (
   ldapServer = "ldap-master.csclub.uwaterloo.ca"
)

type ByTerm []string
func (s ByTerm) Len() int {
   return len(s)
}

func (s ByTerm) Swap(i, j int) {
   s[i], s[j] = s[j], s[i]
}

func (s ByTerm) Less(i, j int) bool {
   a := s[i]
   b := s[j]

   ya, _ := strconv.ParseInt(a[1:5], 10, 32)
   yb, _ := strconv.ParseInt(b[1:5], 10, 32)

   if (ya == yb) {
      // w, s, f (happens to be in reverse order)
      return a[0] > b[0]
   }

   return ya < yb
}

func member(command *bot.Cmd) (string, error) {
   var lines []string

   if len(command.Args) != 1 {
      return "An invalid number of arguments was provided. Usage is: !member userid", nil
   }

   authorized, _ := utils.SyscomNicks()
   if (!utils.InList(authorized, command.User.Nick)) {
      return "Sorry, you are not authorized to request membership information from me.", nil
   }

   l, err := ldap.DialTLS("tcp", fmt.Sprintf("%s:%d", ldapServer, 636), &tls.Config{
      ServerName: ldapServer,
   })
   if err != nil {
      return "", err
   }
   defer l.Close()

   req := ldap.NewSearchRequest("ou=People,dc=csclub,dc=uwaterloo,dc=ca", ldap.ScopeWholeSubtree,
                                ldap.NeverDerefAliases, 0, 0, false,
                                fmt.Sprintf("(&(uid=%s)(objectClass=member))", command.Args[0]),
                                []string{"*"}, nil)

   res, err := l.Search(req)
   if err != nil {
      return "", err
   }

   if len(res.Entries) == 0 {
      return fmt.Sprintf("No members matched userid %q", command.Args[0]), nil
   }

   entry := res.Entries[0]
   lines = append(lines, fmt.Sprintf("%s (%s) -- %s",
                                     entry.GetAttributeValue("uid"),
                                     entry.GetAttributeValue("uidNumber"),
                                     entry.GetAttributeValue("cn")))
   lines = append(lines, entry.GetAttributeValue("program"))

   positions := sort.StringSlice(entry.GetAttributeValues("position")[0:])
   positions.Sort()
   if len(positions) > 0 {
      lines = append(lines, fmt.Sprintf("Position: %s", strings.Join(positions, ", ")))
   }

   // Get groups
   groupReq := ldap.NewSearchRequest("ou=Group,dc=csclub,dc=uwaterloo,dc=ca", ldap.ScopeWholeSubtree,
                                     ldap.NeverDerefAliases, 0, 0, false,
                                     fmt.Sprintf("(&(uniqueMember=%s)(objectClass=group))", entry.DN),
                                     []string{"*"}, nil)
   groupRes, err := l.Search(groupReq)
   if err != nil {
      return "", err
   }
   if len(groupRes.Entries) > 0 {
      groups := make([]string, len(groupRes.Entries))
      for i, entry := range groupRes.Entries {
         groups[i] = entry.GetAttributeValue("cn")
      }

      sgroups := sort.StringSlice(groups[0:])
      sort.Sort(sgroups)
      lines = append(lines, fmt.Sprintf("Clubs/Groups: %s", strings.Join(sgroups, ", ")))
   }

   terms := entry.GetAttributeValues("term")
   sort.Sort(ByTerm(terms))
   if len(terms) > 0 {
      lines = append(lines, fmt.Sprintf("Terms: %s", strings.Join(terms, ", ")))
   }

   nonMemberTerms := entry.GetAttributeValues("nonMemberTerm")
   sort.Sort(ByTerm(terms))
   if len(nonMemberTerms) > 0 {
      lines = append(lines, fmt.Sprintf("Non Member Terms: %s", strings.Join(nonMemberTerms, ", ")))
   }

   lines = append(lines, fmt.Sprintf("Login Shell: %s, Home Directory: %s", entry.GetAttributeValue("loginShell"), entry.GetAttributeValue("homeDirectory")))

   return strings.Join(lines, "\n"), nil
}

func init() {
   bot.RegisterCommand(
      "member",
      "Prints member information",
      "userid",
      member)
}
