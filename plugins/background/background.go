package background

import (
   "os"
   "strings"

   "github.com/go-chat-bot/bot"
)

var Messages chan string = make(chan string, 100)
func processMessages(channel string) (string, error) {
   var lines []string
   for {
      select {
         case message, _ := <- Messages:
            lines = append(lines, message)
         default:
            return strings.Join(lines, "\n"), nil
      }
   }
   return strings.Join(lines, "\n"), nil
}

func init() {
   channels := strings.Split(os.Getenv("SYSCOM_CHANNELS"), ",")

   if len(channels) > 0 {
      config := bot.PeriodicConfig{
         CronSpec: "@every 1s",
         Channels: channels,
         CmdFunc: processMessages,
      }
      bot.RegisterPeriodicCommand("background_messages", config)
   }

}
