package club

import (
   "fmt"
   "strings"

   "git.uwaterloo.ca/csc/cscsysbot/utils"

   "github.com/go-chat-bot/bot"
)

type InsensitiveSlice []string
func (s InsensitiveSlice) Len() int {
   return len(s)
}

func (s InsensitiveSlice) Swap(i, j int) {
   s[i], s[j] = s[j], s[i]
}

func (s InsensitiveSlice) Less(i, j int) bool {
   a := strings.ToLower(s[i])
   b := strings.ToLower(s[j])

   return a < b
}

func ping(command *bot.Cmd) (string, error) {
   nicks, _ := utils.SyscomNicks()
   nicksStr := strings.Join(nicks, ", ")
   if len(command.RawArgs) > 0 && len(command.RawArgs) + len(nicksStr) < 400 {
      return fmt.Sprintf("%s (attn: %s)", command.RawArgs, nicksStr), nil
   } else if len(command.RawArgs) > 0 {
      return fmt.Sprintf("%s\n^ %s", command.RawArgs, nicksStr), nil
   } else {
      return fmt.Sprintf("Ping %s", nicksStr), nil
   }

   return "", nil
}

func init() {
   bot.RegisterCommand(
      "ping",
      "Highlights all Syscom members.",
      "Optionally a message to prepend the list of syscom members.",
      ping)
}
