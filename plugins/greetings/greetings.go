package greetings

import (
   "fmt"
   "os"
   "strings"

   "git.uwaterloo.ca/csc/cscsysbot/utils/uptimerobot"

   "github.com/go-chat-bot/bot"
)

func goodMorning(channel string) (string, error) {
   var lines []string

   lines = append(lines, fmt.Sprintf("Good morning, %s!", channel))

   // Get UptimeRobot status
   monitors, err := uptimerobot.GetMonitors()
   if err == nil {
      statuses := make(map[uptimerobot.MonitorStatus]int)
      for _, mon := range monitors.Monitors {
         _, ok := statuses[mon.Status]
         if !ok {
            statuses[mon.Status] = 1
         } else {
            statuses[mon.Status] += 1
         }
      }

      var sts []string
      for status, num := range statuses {
         sts = append(sts, fmt.Sprintf("%d %s", num, strings.ToLower(status.String())))
      }

      lines = append(lines, fmt.Sprintf("Uptime Robot Monitors: %s", strings.Join(sts, ", ")))
   } else {
      lines = append(lines, "Unable to get Uptime Robot information")
   }

   return strings.Join(lines, "\n"), nil
}

func init() {
   channels := strings.Split(os.Getenv("SYSCOM_CHANNELS"), ",")

   if len(channels) > 0 {
      config := bot.PeriodicConfig{
         CronSpec: "0 0 8 * * mon-fri",
         Channels: channels,
         CmdFunc: goodMorning,
      }
      bot.RegisterPeriodicCommand("good_morning", config)
   }
}
