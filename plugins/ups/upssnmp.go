package ups

import(
   "fmt"
   "time"

   "github.com/alouca/gosnmp"
)

type BatteryStatus int
const (
   BatteryStatusUnknown BatteryStatus = 1
   BatteryStatusNormal BatteryStatus = 2
   BatteryStatusLow BatteryStatus = 3
   BatteryStatusFault BatteryStatus = 4
)

func (st BatteryStatus) String() string {
   str := "Invalid"
   switch(st) {
      case BatteryStatusUnknown:
         str = "Unknown"
      case BatteryStatusNormal:
         str = "Normal"
      case BatteryStatusLow:
         str = "Low"
      case BatteryStatusFault:
         str = "Fault"
   }

   return str
}

type BatteryReplaceIndicator int
const (
   BatteryReplaceIndicatorOK BatteryReplaceIndicator = 1
   BatteryReplaceIndicatorReplace BatteryReplaceIndicator = 2
)

func (st BatteryReplaceIndicator) String() string {
   str := "Invalid"
   switch(st) {
      case BatteryReplaceIndicatorOK:
         str = "Ok"
      case BatteryReplaceIndicatorReplace:
         str = "Replace"
   }

   return str
}

type OutputStatus int
const (
   OutputStatusUnknown OutputStatus = 1
   OutputStatusOnLine OutputStatus = 2
   OutputStatusOnBattery OutputStatus = 3
   OutputStatusOnSmartBoost OutputStatus = 4
   OutputStatusTimedSleeping OutputStatus = 5
   OutputStatusSoftwareBypass OutputStatus = 6
   OutputStatusOff OutputStatus = 7
   OutputStatusRebooting OutputStatus = 8
   OutputStatusSwitchedBypass OutputStatus = 9
   OutputStatusHardwareFailureBypass OutputStatus = 10
   OutputStatusSleepingUntilPowerReturn OutputStatus = 11
   OutputStatusOnSmartTrim OutputStatus = 12
)

func (st OutputStatus) String() string {
   str := "Invalid"
   switch(st) {
      case OutputStatusUnknown:
         str = "Unknown"
      case OutputStatusOnLine:
         str = "On Line"
      case OutputStatusOnBattery:
         str = "On Battery"
      case OutputStatusOnSmartBoost:
         str = "On Smart Boost"
      case OutputStatusTimedSleeping:
         str = "Timed Sleeping"
      case OutputStatusSoftwareBypass:
         str = "Software Bypass"
      case OutputStatusOff:
         str = "Off"
      case OutputStatusRebooting:
         str = "Rebooting"
      case OutputStatusSwitchedBypass:
         str = "Switched Bypass"
      case OutputStatusHardwareFailureBypass:
         str = "Hardware Failure Bypass"
      case OutputStatusSleepingUntilPowerReturn:
         str = "Sleeping Until Power Return"
      case OutputStatusOnSmartTrim:
         str = "On Smart Trim"
   }

   return str
}

func getRuntime(s *gosnmp.GoSNMP) (* time.Duration, error) {
   resp, err := s.Get("1.3.6.1.4.1.318.1.1.1.2.2.3.0")
   if err != nil {
      return nil, err
   }

   if len(resp.Variables) == 0 {
      return nil, nil
   }

   time, _ := time.ParseDuration(fmt.Sprintf("%dm", resp.Variables[0].Value.(int) / 60/ 100))
   return &time, nil
}

func getBatteryStatus(s *gosnmp.GoSNMP) (* BatteryStatus, error) {
   resp, err := s.Get("1.3.6.1.4.1.318.1.1.1.2.1.1.0")
   if err != nil {
      return nil, err
   }

   if len(resp.Variables) == 0 {
      return nil, nil
   }

   status := BatteryStatus(resp.Variables[0].Value.(int))
   return &status, nil
}

func getBatteryReplaceIndicator(s *gosnmp.GoSNMP) (* BatteryReplaceIndicator, error) {
   resp, err := s.Get("1.3.6.1.4.1.318.1.1.1.2.2.4.0")
   if err != nil {
      return nil, err
   }

   if len(resp.Variables) == 0 {
      return nil, nil
   }

   status := BatteryReplaceIndicator(resp.Variables[0].Value.(int))
   return &status, nil
}

func getOutputStatus(s *gosnmp.GoSNMP) (* OutputStatus, error) {
   resp, err := s.Get("1.3.6.1.4.1.318.1.1.1.4.1.1.0")
   if err != nil {
      return nil, err
   }

   if len(resp.Variables) == 0 {
      return nil, nil
   }

   status := OutputStatus(resp.Variables[0].Value.(int))
   return &status, nil
}

type UPS struct {
   Name string
   EstimatedRuntime *time.Duration
   BatteryStatus *BatteryStatus
   BatteryReplaceIndicator *BatteryReplaceIndicator
   OutputStatus *OutputStatus
}

func UPSStatus(name string, community string) *UPS {
   ups := &UPS{
      Name: name,
      EstimatedRuntime: nil,
      BatteryStatus: nil,
      BatteryReplaceIndicator: nil,
      OutputStatus: nil,
   }

   // Get UPS status
   snmp, _ := gosnmp.NewGoSNMP(name, community, gosnmp.Version1, 2)

   ups.EstimatedRuntime, _ = getRuntime(snmp)
   ups.BatteryStatus, _ = getBatteryStatus(snmp)
   ups.BatteryReplaceIndicator, _ = getBatteryReplaceIndicator(snmp)
   ups.OutputStatus, _ = getOutputStatus(snmp)

   return ups
}
