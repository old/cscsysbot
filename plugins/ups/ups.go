package ups

import (
   "fmt"
   "strings"
   "os"

   "github.com/go-chat-bot/bot"
)

func ups(command *bot.Cmd) (msg string, err error) {
   upses := strings.Split(os.Getenv("UPSES"), ",")

   var messages []string
   for _, upsName := range upses {
      ups := UPSStatus(upsName, os.Getenv("UPS_COMMUNITY_STRING"))
      messages = append(messages, fmt.Sprintf("[%s] Runtime: %s, Status: %s, Battery: %s - %s\n", strings.Split(ups.Name, ".")[0], ups.EstimatedRuntime, ups.OutputStatus, ups.BatteryStatus, ups.BatteryReplaceIndicator))
   }

   msg = strings.Join(messages, "\n")
   return
}

func init() {
   bot.RegisterCommand(
      "ups",
      "Prints current UPS status information. (Note: this command may take some time to execute)",
      "",
      ups)
}
