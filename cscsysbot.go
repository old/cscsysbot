package main

import (
   "os"
   "strings"
   "fmt"

   "github.com/go-chat-bot/bot/irc"

   // Plugins
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/hello"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/url"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/ups"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/member"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/club"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/ping"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/greetings"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/personality"

   // Backgrounds tasks
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/background"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/tweets"
   _ "git.uwaterloo.ca/csc/cscsysbot/plugins/uptimerobot"
)

func main() {
   _, debug := os.LookupEnv("IRC_DEBUG")

   irc.Run(&irc.Config{
      Server:  fmt.Sprintf("%s:%s", os.Getenv("IRC_SERVER"), os.Getenv("IRC_PORT")),
      Channels:  strings.Split(os.Getenv("IRC_CHANNELS"), ","),
      User:    os.Getenv("IRC_USER"),
      Nick:    os.Getenv("IRC_NICK"),
      Password: os.Getenv("IRC_PASSWORD"),
      UseTLS: true,
      TLSServerName: os.Getenv("IRC_SERVER"),
      Debug: debug,
   })
}
